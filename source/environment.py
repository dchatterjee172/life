from itertools import product
from components.animal import Bunny, Fox
from components.vegetation import Plant
from brain import BrainManager
from random import choices, random, shuffle
import numpy as np


class Env(dict):
    orient_to_accessible_neighbour = {
        0: ((-1, 0), (-1, 2)),
        90: ((-1, 2), (1, 2)),
        180: ((1, 2), (-1, 2)),
        270: ((-1, 2), (-1, 0)),
    }

    def __init__(
        self,
        all_components: tuple,
        max_x: int,
        max_y: int,
        brain_manager: BrainManager = None,
        time: int = None,
        cum_weights: tuple = (0.33, 0.66, 1.0),
    ):
        dict.__init__(self)
        if len(all_components) != len(cum_weights):
            raise ValueError("all_stuff, cum_weights should have same length")
        self.time = -1 if time is None else time
        self.max_x = max_x
        self.max_y = max_y
        self.all_components = all_components
        self.brain_manager = (
            BrainManager(max_workers=3, queue_length=max_x * max_y * 2)
            if brain_manager is None
            else brain_manager
        )
        if time is None:
            self._populate(cum_weights)

    def _populate(self, cum_weights: tuple):
        self.time += 1
        for (x, y), cls in zip(
            product(range(self.max_x), range(self.max_y)),
            choices(
                self.all_components, cum_weights=cum_weights, k=self.max_x * self.max_y
            ),
        ):
            if random() > 0.7:
                obj = cls(x=x, y=y)
                self[x, y] = obj

    def _surrounding(self, x: int, y: int, x_range: tuple, y_range: tuple):
        for delta_x, delta_y in product(range(*x_range), range(*y_range)):
            new_x = (x + delta_x) % self.max_x
            new_y = (y + delta_y) % self.max_y
            if new_x == x and new_y == y:
                continue
            if (new_x, new_y) not in self:
                yield None, (new_x, new_y)
            else:
                yield self[new_x, new_y], (new_x, new_y)

    def _get_sensor_data(self, obj):
        sensor_data = []
        if obj.orient_degree == 0:
            x_range = (-obj.vision_scale - 1, obj.vision_scale + 1)
            y_range = (-obj.vision_scale, obj.vision_scale + 1)
        elif obj.orient_degree == 90:
            x_range = (-obj.vision_scale, obj.vision_scale + 1)
            y_range = (-obj.vision_scale, obj.vision_scale + 2)
        elif obj.orient_degree == 180:
            x_range = (-obj.vision_scale, obj.vision_scale + 2)
            y_range = (-obj.vision_scale, obj.vision_scale + 1)
        else:
            x_range = (-obj.vision_scale, obj.vision_scale + 1)
            y_range = (-obj.vision_scale - 1, obj.vision_scale + 1)
        for obj, _ in self._surrounding(*obj.coord, x_range=x_range, y_range=y_range):
            if obj is None:
                cell = (0.0, 0.0, 0.0, 0.0)
            elif isinstance(obj, Fox):
                cell = (1.0, 0.0, 0.0, obj.gender)
            elif isinstance(obj, Bunny):
                cell = (0.0, 1.0, 0.0, obj.gender)
            elif isinstance(obj, Plant):
                cell = (0.0, 0.0, 1.0, obj.gender)
            sensor_data.append(cell)
        return np.array(sensor_data).flatten()

    @staticmethod
    def _compute(env):
        next_env = Env(
            time=env.time,
            max_x=env.max_x,
            max_y=env.max_y,
            all_components=env.all_components,
            brain_manager=env.brain_manager,
        )
        for obj in sorted(env.values(), key=lambda obj: obj.life_force, reverse=True):
            if obj.life_force < 1:
                if np.random.uniform(0, 1) < 0.7:
                    next_env[obj.coord] = Plant(*obj.coord)
                continue
            next_env[obj.coord] = obj
            if isinstance(obj, Plant):
                continue
            x_range, y_range = Env.orient_to_accessible_neighbour[obj.orient_degree]
            surrounding = list(
                env._surrounding(*obj.coord, x_range=x_range, y_range=y_range)
            )
            shuffle(surrounding)
            eaten = False
            for s_obj, coord in surrounding:
                if s_obj is None:
                    if obj.baby is not None:
                        baby = type(obj)(
                            x=coord[0],
                            y=coord[1],
                            brain=obj.baby,
                            initial_hunger=obj.hunger,
                        )
                        next_env[coord] = baby
                        obj.baby = None
                elif isinstance(s_obj, obj.eats) and not eaten:
                    obj.eat()
                    s_obj.reduce_health()
                    eaten = True
                elif (
                    obj.gender == 0
                    and isinstance(s_obj, type(obj))
                    and s_obj.gender == 1
                ):
                    obj.get_pregnant(s_obj.brain)

            if next_env.time % 10 == 0 and next_env.time > 1:
                next_env.brain_manager.train(obj._id, obj.brain, 100, 0.1)
        return next_env

    @staticmethod
    def _move(env):
        next_env = Env(
            time=env.time + 1,
            max_x=env.max_x,
            max_y=env.max_y,
            all_components=env.all_components,
            brain_manager=env.brain_manager,
        )
        for obj in sorted(env.values(), key=lambda obj: obj.life_force, reverse=True):
            if isinstance(obj, Plant):
                next_env[obj.coord] = obj
                continue
            surrounding = env._get_sensor_data(obj)
            neg_log_proba = -obj.next_step(surrounding)
            current_x, current_y = obj.coord
            predict_orient = 90 * np.argmin(neg_log_proba)
            if predict_orient == 0:
                new_x = (current_x - 1) % env.max_x
                new_y = current_y
            elif predict_orient == 90:
                new_x = current_x
                new_y = (current_y + 1) % env.max_y
            elif predict_orient == 180:
                new_x = (current_x + 1) % env.max_x
                new_y = current_y
            else:
                new_x = current_x
                new_y = (current_y - 1) % env.max_y
            if (new_x, new_y) not in next_env and (new_x, new_y) not in env:
                next_env[new_x, new_y] = obj
                obj.coord_set(coord=(new_x, new_y), predict_orient=predict_orient)
            else:
                obj.coord_set(coord=obj.coord, predict_orient=predict_orient)
                next_env[current_x, current_y] = obj
        return next_env

    def next_step(self):
        next_env = Env._move(self)
        next_env = Env._compute(next_env)
        return next_env
