import numpy as np
from functools import partial
from collections import namedtuple
from multiprocessing import get_context
import jax.numpy as jnp
from jax import grad, jit, random
import redis
from pickle import dumps
from signal import signal, SIGTERM
from queue import Empty


class Compute(namedtuple("Compute", ("w1", "b1", "w2", "b2"))):
    def __add__(self, other):
        return Compute(
            w1=self.w1 + other.w1,
            b1=self.b1 + other.b1,
            w2=self.w2 + other.w2,
            b2=self.b2 + other.b2,
        )

    def __sub__(self, other):
        return Compute(
            w1=self.w1 - other.w1,
            b1=self.b1 - other.b1,
            w2=self.w2 - other.w2,
            b2=self.b2 - other.b2,
        )

    def __mul__(self, scalar: float):
        return Compute(
            w1=self.w1 * scalar,
            b1=self.b1 * scalar,
            w2=self.w2 * scalar,
            b2=self.b2 * scalar,
        )

    __rmul__ = __mul__


class Memory(namedtuple("Memory", ("sensor_data", "labels", "reward_delta"))):
    def __add__(self, other):
        sensor_data = np.concatenate(
            (
                self.sensor_data[self.reward_delta > 0],
                other.sensor_data[other.reward_delta > 0],
            ),
            axis=0,
        )
        labels = np.concatenate(
            (self.labels[self.reward_delta > 0], other.labels[other.reward_delta > 0]),
            axis=0,
        )

        reward_delta = np.concatenate(
            (
                self.reward_delta[self.reward_delta > 0],
                other.reward_delta[other.reward_delta > 0],
            ),
            axis=0,
        )
        return Memory(sensor_data=sensor_data, labels=labels, reward_delta=reward_delta)

    def get_index(self, index):
        if isinstance(index, slice):
            return Memory(
                sensor_data=self.sensor_data[index.start : index.stop],  # noqa: E203
                labels=self.labels[index.start : index.stop],  # noqa: E203
                reward_delta=self.reward_delta[index.start : index.stop],  # noqa: E203
            )
        return Memory(
            sensor_data=np.take(self.sensor_data, index, 0),
            labels=np.take(self.labels, index, 0),
            reward_delta=np.take(self.reward_delta, index, 0),
        )

    def _extend_with_zeros(self, num: int):
        sensor_data = np.concatenate(
            (self.sensor_data, jnp.zeros(shape=(num, self.sensor_data.shape[1]))),
            axis=0,
        )
        labels = np.concatenate(
            (self.labels, jnp.zeros(shape=(num, self.labels.shape[1]))), axis=0
        )

        reward_delta = np.concatenate(
            (self.reward_delta, jnp.zeros(shape=(num,))), axis=0
        )
        return Memory(sensor_data=sensor_data, labels=labels, reward_delta=reward_delta)

    def _set_new_experience(self, sensor_data_one, label, reward_delta_one):
        min_index = np.argmin(self.reward_delta)
        if (
            np.abs(reward_delta_one) < self.reward_delta.min()
            and np.random.uniform(0, 1) > 0.1
        ):
            return self
        sensor_data, labels, reward_delta = self
        sensor_data[min_index, ...] = sensor_data_one
        labels[min_index, ...] = (
            label if reward_delta_one > 0 else (1 - label) * 1 / (label.size - 1)
        )
        reward_delta[min_index] = np.abs(reward_delta_one)
        return Memory(sensor_data=sensor_data, labels=labels, reward_delta=reward_delta)


class Brain(namedtuple("Brain", ("Compute", "Memory"))):
    def __add__(self, other):
        raise NotImplementedError()

    def set_new_experience(self, sensor_data_one, label, reward_delta):
        return Brain(
            Compute=self.Compute,
            Memory=self.Memory._set_new_experience(
                sensor_data_one, label, reward_delta
            ),
        )


def get_compute(input_size: int, hidden_size: int, output_size: int, seed: int):
    key = random.PRNGKey(seed)
    w1 = random.truncated_normal(
        key, lower=-0.1, upper=0.1, shape=(input_size, hidden_size)
    )
    w2 = random.truncated_normal(
        key, lower=-0.1, upper=0.1, shape=(hidden_size, output_size)
    )
    b1 = jnp.zeros(shape=(hidden_size,))
    b2 = jnp.zeros(shape=(output_size,))
    return Compute(w1=w1, b1=b1, w2=w2, b2=b2)


def get_memory(max_memory: int, input_size: int, output_size: int):
    sensor_data = np.zeros(shape=(max_memory, input_size))
    labels = np.zeros(shape=(max_memory, output_size))
    reward_delta = np.zeros(shape=(max_memory,))
    return Memory(sensor_data=sensor_data, labels=labels, reward_delta=reward_delta)


def get_brain(
    input_size: int, hidden_size: int, output_size: int, max_memory: int, seed: int
):
    return Brain(
        Compute=get_compute(
            input_size=input_size,
            hidden_size=hidden_size,
            output_size=output_size,
            seed=seed,
        ),
        Memory=get_memory(
            max_memory=max_memory, input_size=input_size, output_size=output_size
        ),
    )


@jit
def forward(compute: Compute, data: np.ndarray):
    o1 = jnp.matmul(data, compute.w1) + compute.b1
    a1 = jnp.tanh(o1)
    o2 = jnp.matmul(a1, compute.w2) + compute.b2
    a2 = o2 - jnp.expand_dims(jnp.log(jnp.exp(o2).sum(axis=1)), 1)
    return a2


def worker(queue, worker_id):
    rd = redis.Redis()
    flag = True

    def handle(*args):
        nonlocal flag
        flag = False

    signal(SIGTERM, handle)

    def forward(compute: Compute, data: np.ndarray):
        o1 = jnp.matmul(data, compute.w1) + compute.b1
        a1 = jnp.tanh(o1)
        o2 = jnp.matmul(a1, compute.w2) + compute.b2
        a2 = o2 - jnp.log(jnp.exp(o2).sum(axis=1, keepdims=True))
        return a2

    def loss(compute: Compute, data: np.ndarray, labels: np.ndarray, non_zero_num: int):
        pred = forward(compute, data)
        loss = jnp.sum(-(labels * pred).sum(1)) / non_zero_num
        return loss

    g_loss = grad(loss)

    while flag:
        try:
            data_tuple = queue.get(timeout=1)
        except Empty:
            continue
        id_, compute, data, labels, non_zero_num, epoch, learning_rate = data_tuple
        grad_loss = partial(g_loss, data=data, labels=labels, non_zero_num=non_zero_num)

        @jit
        def sgd(compute: Compute, learning_rate: float):
            g = grad_loss(compute)
            return compute - g * learning_rate

        for i in range(epoch):
            if not flag:
                break
            compute = sgd(compute, learning_rate)
        rd.set(id_, dumps(compute), ex=60)
        del data_tuple, grad_loss, compute, data, labels


class BrainManager:
    def __init__(self, max_workers: int, queue_length: int):
        self.queue_length = queue_length
        self.ctx = get_context("spawn")
        self.queue = self.ctx.Queue(queue_length)
        self.max_workers = max_workers
        self.workers = []
        self.spawn_worker()

    def spawn_worker(self):
        print("Spawning worker!")
        p = self.ctx.Process(target=worker, args=(self.queue, len(self.workers)))
        p.start()
        self.workers.append(p)

    def train(self, id_: str, brain: Brain, epoch: int, learning_rate: float):
        payload = (
            id_,
            brain.Compute,
            brain.Memory.sensor_data,
            brain.Memory.labels,
            jnp.count_nonzero(brain.Memory.reward_delta),
            epoch,
            learning_rate,
        )
        self.queue.put(payload)
        if (
            self.queue.qsize() / self.queue_length > 0.9
            and len(self.workers) < self.max_workers
        ):
            self.spawn_worker()

    def delete(self):
        for w in self.workers:
            w.terminate()
        for w in self.workers:
            w.join()
            w.close()
        try:
            while True:
                self.queue.get_nowait()
        except Empty:
            pass
        self.queue.close()
        self.queue.join_thread()


if __name__ == "__main__":
    from time import monotonic

    bm = BrainManager(max_workers=2, queue_length=10)
    brain = get_brain(100, 200, 9, 1000, 1)
    start = monotonic()
    for i in range(2000):
        brain = brain.set_new_experience(
            np.random.normal(size=(100,)),
            np.random.uniform(0, 1, (9,)),
            np.random.uniform(-10, 10),
        )
    print((monotonic() - start) / 2000)
    start = monotonic()
    for i in range(20):
        bm.train(str(i), brain, 1000, 1)
    print((monotonic() - start) / 20)
