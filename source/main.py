from components import Plant, Bunny, Fox
from environment import Env
import sys
import multiprocessing as mp
from signal import signal, SIGTERM, SIGINT, SIG_IGN
from collections import defaultdict
import pyglet
from pyglet import gl
from pathlib import Path
from queue import Empty, Full
from time import sleep
import gc
from pprint import pprint


def run_env(queue, max_x, max_y, all_components, cum_weights):
    signal(SIGINT, SIG_IGN)
    run = True

    def handle(*args):
        nonlocal run
        run = False

    signal(SIGTERM, handle)
    env = Env(
        all_components=all_components, cum_weights=cum_weights, max_x=max_x, max_y=max_y
    )
    components = None
    while run:
        if components is None:
            env = env.next_step()
            components = {
                obj.coord: (type(obj).__name__, obj.orient_degree)
                for obj in env.values()
            }
        try:
            queue.put(components, timeout=0.5)
            components = None
        except Full:
            pass
    env.brain_manager.delete()
    try:
        while True:
            queue.get_nowait()
    except Empty:
        pass
    queue.close()
    queue.join_thread()


if __name__ == "__main__":
    mp.freeze_support()
    debug = False
    max_x, max_y = 7, 7
    all_components = (Plant, Bunny, Fox)
    cum_weights = (0.5, 0.9, 1)
    if debug:
        env = Env(
            all_components=all_components,
            cum_weights=(0.7, 0.8, 1),
            max_x=max_x,
            max_y=max_y,
        )
        for i in range(10000):
            env = env.next_step()
            counts = defaultdict(float)
            for obj in env.values():
                counts[type(obj)] += obj.life_force
            pprint(counts)
            sleep(0.2)
        sys.exit(0)

    sprite_path = Path("./sprites")
    all_sprites = dict()
    for component in all_components:
        name = component.__name__
        all_sprites[name] = pyglet.image.load(sprite_path / f"{name}.png")
    all_sprites["default"] = pyglet.image.load(sprite_path / f"default.png")
    batch = pyglet.graphics.Batch()
    background = pyglet.graphics.OrderedGroup(0)
    foreground = pyglet.graphics.OrderedGroup(1)
    window = pyglet.window.Window(fullscreen=True)
    ctx = mp.get_context("spawn")
    queue = ctx.Queue(50)
    env_p = ctx.Process(
        target=run_env, args=(queue, max_x, max_y, all_components, cum_weights)
    )
    env_p.start()

    def cleanup():
        window.close()
        env_p.terminate()
        if env_p.is_alive():
            env_p.join()
        env_p.close()

    scale = 2
    pause = False
    last_components = None
    last_board = None
    screen_height, screen_width = window.height, window.width

    @window.event
    def on_key_press(symbol, modifiers):
        global scale, pause
        if symbol == pyglet.window.key.Z:
            scale = 2 if scale == 1 else 1
        elif symbol == pyglet.window.key.SPACE:
            pause = not pause

    @window.event
    def on_draw():
        global queue, scale, last_components, max_x, max_y
        try:
            if not pause:
                components = queue.get_nowait()
                del last_components
                last_components = components
            else:
                components = last_components
        except Empty:
            return
        window.clear()
        new_board = []
        for sprite in all_sprites.values():
            if sprite != "default":
                sprite.anchor_x = sprite.width // 2
                sprite.anchor_y = sprite.height // 2
        board = [
            pyglet.sprite.Sprite(
                all_sprites["default"],
                (y + 0.5) * all_sprites["default"].height * scale,
                (x + 0.5) * all_sprites["default"].width * scale,
                batch=batch,
                group=background,
            )
            for y in range(max_y)
            for x in range(max_x)
        ]
        for (x, y), (component, degree) in components.items():
            gl.glEnable(gl.GL_TEXTURE_2D)
            gl.glTexParameteri(
                gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST
            )
            sprite = pyglet.sprite.Sprite(
                all_sprites[component],
                int((y + 0.5) * scale * all_sprites[component].height),
                int((x + 0.5) * scale * all_sprites[component].width),
                batch=batch,
                group=foreground,
            )
            sprite.update(
                scale_x=scale,
                scale_y=scale,
                rotation=(
                    abs(180 - degree) if degree == 0 or degree == 180 else degree
                ),
            )
            new_board.append(sprite)
        for x in board:
            x.update(scale_x=scale, scale_y=scale)
        batch.draw()
        del board
        gc.collect()

    @window.event
    def on_close():
        cleanup()
        sys.exit(0)

    try:
        while True:
            sleep(0.8)
            pyglet.clock.tick()

            for window in pyglet.app.windows:
                window.switch_to()
                window.dispatch_events()
                window.dispatch_event("on_draw")
                window.flip()
    except KeyboardInterrupt:
        cleanup()
        sys.exit(0)
