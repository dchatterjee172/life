from components.vegetation import Plant
from brain import get_brain, forward, Brain
from random import randint
import numpy as np
import redis
from pickle import loads
import uuid

rd = redis.Redis()


class Animal:
    def __init__(
        self,
        x: int,
        y: int,
        eats: str,
        brain: Brain = None,
        delta_hunger: float = 0.1,
        initial_hunger: float = 1.0,
        baby_cool_down: int = 10,
    ):
        self.x = x
        self.y = y
        self._id = str(uuid.uuid4())
        self.gender = float(randint(0, 1))  # 0 -> male 1 -> female
        self.hunger = 1
        self.health = 10 / initial_hunger
        self.vision_scale = 3
        self.orient_degree = np.random.choice((0, 90, 180, 270), 1)
        self.eats = eats
        self.output_shape = (4,)
        self.brain = (
            get_brain(
                input_size=4 * (self.vision_scale * 2 + 1) ** 2
                - 4
                + 4 * ((self.vision_scale + 1) * 2 + 1)
                - 2 * 4,
                hidden_size=50,
                output_size=self.output_shape[0],
                max_memory=1000,
                seed=randint(0, 1e10),
            )
            if brain is None
            else brain
        )
        self.last_movement = None
        self.last_sensor_data = None
        self.baby = None
        self.baby_cool_down = baby_cool_down
        self.last_baby_step = 0
        self.age = 0
        self.delta_hunger = delta_hunger

    @property
    def coord(self):
        return self.x, self.y

    def coord_set(self, coord, predict_orient):
        movement = np.zeros(shape=self.output_shape)
        movement[predict_orient // 90] = 1
        movement = movement.flatten()
        self.last_movement = movement
        self.x, self.y = coord
        self.orient_degree = predict_orient

    @property
    def life_force(self):
        return 2 * self.health / (self.hunger ** 2)

    def get_pregnant(self, mate: Brain):
        if self.age - self.last_baby_step < self.baby_cool_down or self.age < 30:
            return
        r = np.random.uniform(0, 1)
        compute = r * self.brain.Compute + (1 - r) * mate.Compute
        memory = self.brain.Memory + mate.Memory
        length = memory.sensor_data.shape[0]
        indexes = np.random.choice(
            length, 1000 if length > 1000 else length, replace=False
        )
        if len(indexes) > 0:
            memory = memory.get_index(indexes)
        if length < 1000:
            memory = memory._extend_with_zeros(1000 - length)
        self.baby = Brain(Memory=memory, Compute=compute)
        self.last_baby_step = self.age

    def send_to_memory(self, reward_delta):
        if self.last_sensor_data is not None and self.last_movement is not None:
            self.brain = self.brain.set_new_experience(
                self.last_sensor_data, self.last_movement, reward_delta
            )

    def eat(self):
        life_force_before = self.life_force
        new_hunger = self.hunger - self.delta_hunger * 10
        self.hunger = new_hunger if new_hunger > 1 else 1
        delta_life_force = self.life_force - life_force_before
        self.send_to_memory(delta_life_force)

    def reduce_health(self):
        life_force_before = self.life_force
        self.health = self.health - 1 if self.health > 1 else 0
        delta_life_force = self.life_force - life_force_before
        self.send_to_memory(delta_life_force)

    def increase_health(self):
        life_force_before = self.life_force
        self.health = self.health + 1 if self.health > 1 else 0
        delta_life_force = self.life_force - life_force_before
        self.send_to_memory(delta_life_force)

    def next_step(self, surrounding):
        self.age += 1
        self.last_sensor_data = surrounding.copy()
        self.last_movement = None
        self.hunger += self.delta_hunger
        surrounding = np.expand_dims(surrounding, 0)
        compute = rd.get(self._id)
        if compute is not None:
            self.brain = Brain(Compute=loads(compute), Memory=self.brain.Memory)
            rd.delete(self._id)
        return (
            np.reshape(forward(self.brain.Compute, surrounding), self.output_shape)
            if np.random.uniform(0, 1) > 1 / np.log10(self.age + 10)
            else np.random.normal(size=self.output_shape)
        )


class Fox(Animal):
    count = 0

    def __init__(
        self, x: int, y: int, brain: Brain = None, initial_hunger: float = 1.0
    ):
        Animal.__init__(
            self,
            x=x,
            y=y,
            eats=Bunny,
            brain=brain,
            delta_hunger=0.1,
            initial_hunger=initial_hunger,
            baby_cool_down=10,
        )
        Fox.count += 1

    def __del__(self):
        Fox.count -= 1


class Bunny(Animal):
    count = 0

    def __init__(
        self, x: int, y: int, brain: Brain = None, initial_hunger: float = 1.0
    ):
        Animal.__init__(
            self,
            x=x,
            y=y,
            eats=Plant,
            brain=brain,
            delta_hunger=1,
            initial_hunger=initial_hunger,
            baby_cool_down=3,
        )
        Bunny.count += 1

    def __del__(self):
        Bunny.count -= 1
