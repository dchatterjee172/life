class Vegetation:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y
        self.gender = -1.0
        self.health = 10
        self.orient_degree = 0

    @property
    def coord(self):
        return self.x, self.y

    @property
    def life_force(self):
        return self.health

    def reduce_health(self):
        self.health = self.health - 1 if self.health > 1 else 0


class Plant(Vegetation):
    count = 0

    def __init__(self, x: int, y: int):
        Vegetation.__init__(self, x, y)
        Plant.count += 1

    def __del__(self):
        Plant.count -= 1
