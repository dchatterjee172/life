poetry install --no-dev
docker stop redis_life
docker rm redis_life
docker run -d -p 6379:6379 --name redis_life redis
sleep 5
poetry run python ./source/main.py
docker stop redis_life
docker rm redis_life
# kill -9 $(pgrep -f pypoetry)
