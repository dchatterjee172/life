* Simulate an environment.
* The environment will contain predators and prey.
* The environment and the stuff inside the environment will follow a set of rules.
* The predators and prey will learn to survive in the environment. 

![](gif/readme.gif)
